# 2. Write a function in Python to count uppercase characters in a text file.

def count_letter():
    f = open("a_1.txt","r")
    text_1 = f.read()
    count = 0
    for letter in text_1:
        if letter.isupper():
            count += 1
    print(count)
    f.close()

count_letter()
