# 1. Write a Python program to square and cube every number in a given list of integers using Lambda.
# Sample
# ([1,2,3,4,5])
# Output:
# ([1, 4, 9, 16, 25], [1, 8, 27, 64, 125])

l = [17, 12, 3, 4, 7, 19]

result = list(map(lambda x: x ** 2, l)), list(map(lambda x: x ** 3, l))
print(result)