# 6.Comparison
# Input two positive integers, and output a line describing their relation. Follow the sample format.
# Sample Input
# 7 9
# 11 11
# 4 -4
# Sample Output
# 7 < 9
# 11 = 11
# 4 > -4

# 8 8
# 17 19
# 12 -12

a = 8
b = 8
a = b
print(a = b)

a = 17
b = 19
a < b
print(a < b)

a = 12
b = -12
a > b
print(a > b)
