# DICTIONARIES
# 2.Write a python program which concat 2 dicts.

dic_1 = {'name_1': 'Hello', 'name_2': 'World'}
dic_2 = {'gallahad': 'the pure', 'robin': 'the brave'}

print(dic_1)
print(dic_2)

dic_1['gallahad'] = 'the pure'
dic_1['robin'] = 'the brave'
print(dic_1)


