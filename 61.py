# 2.Largest
# Write a Python program to get the largest number from a list.

# Sample Input

# [1, 2, 3, 4]
# [5, 7, 3, 9, 11]
# [25, 1, 1, 13]
# [1, 2, 1, 1]

# Sample Output

# 4
# 11
# 25
# 2

# Python program to find the largest
# number in a list

# list of numbers
list1 = [10, 20, 4, 45, 99]

# printing the maximum element
print("Largest element is:", max(list1))
