# Comprehensions
# 5. Given dictionary is consisted of vehicles and their weights in kilograms. Construct a list of the names of vehicles with weight below 5000 kilograms. In the same list comprehension make the key names all upper case.

dict_1 = {'passenger': 3587, 'truck': 8910, 'bus': 7264}
dict_2 = {k: v for k,v in dict_1.items() if v < 5000}
print(dict_2)