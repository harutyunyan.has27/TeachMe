# Import modules
# 4. Random Lottery Pick. Generate 100 random lottery tickets and pick two lucky tickets from it as a winner.
# Note you must adhere to the following conditions:
# ● The lottery number must be 10 digits long.
# ● All 100 ticket number must be unique.

import random

lottery_tickets_list = []
print("creating 100 random lottery tickets")

for i in range(100):
    lottery_tickets_list.append(random.randrange(3333333333, 7777777777))

winners = random.sample(lottery_tickets_list, 2)
print("Lucky 2 lottery tickets ", winners)