# Input two integers and print out their sum. Preserve the exact format from the examples(e.g. the output should contain exactly three lines)
# Sample Input 
# 4, 5 
# Sample Output 
# a = 4
# b = 5
# a + b = 4 + 5 = 9

# str = input(17, 19)
a = 17
b = 19
c = a + b
print(c)

# Sample Input
# 17, 8
# Sample Output
# a = 17 
# b = 8
# a + b = 17 + 8 = 25

# str = input(73, 137)
a = 73
b = 137
c = a + b
print(c)
