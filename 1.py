()# 1.Arrange
#  Arrange string characters such that lowercase letters should come first
# Sample Input
# PyNaTive
# Sample Output
# yaivePNT

# Python3 code to demonstrate working of
# Lowercase first character of String
# Using lower() + string slicing

# initializing string
str_1 = "GeeksforGeeks"

# printing original string
print("The original string is : " + str(str_1))

# Using lower() + string slicing
# Lowercase first character of String
str_2 = str_1[0].lower() + str_1[1:]

# printing result
print('The string after lowercasing initial character : ' + str(str_2))