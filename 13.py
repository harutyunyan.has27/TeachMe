# FUNCTIONS
# 4. Given some integer, find the maximal number you can obtain by deleting exactly one digit of the given number.
# ● For n = 152, the output should be
# deleteDigit(n) = 52;
# ● For n = 1001, the output should be
# deleteDigit(n) = 101.

a = 84573570
b = 1

def maxnumber(a, b):
    for i in range(0, b):
        c = 0
        i = 1

        while a // i >= 0:
            d = (a // (i * 10)) * i + (a % i)
            i *= 10

            if d > c:
                c = d

                a = c

                return c

print(maxnumber(a, b))
