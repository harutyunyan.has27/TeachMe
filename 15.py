# 5. Write a Python program to find the intersection of two given arrays using Lambda.
# Original arrays:
# [1, 2, 3, 5, 7, 8, 9, 10]
# [1, 2, 4, 8, 9]
# The intersection of the said arrays: [1, 2, 8, 9]

def iterable(a, b):
    result = list(filter(lambda x: x in a, b))
    print(result)


a = [1, 3, 4, 5, 17, 12, 19 , 38, 7, 98, 36, 5679]
b = [2, 3, 6, 17, 19, 12, 4, 7]
iterable(a, b)