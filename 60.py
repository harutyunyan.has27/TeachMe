# 1.Multiply

# Write a Python program to multiply all the items in a list.

# Sample Input
# [1, 2, 3, 4]
# [5, 7, 3, 9, 11]
# [25, 1, 1, 13]

# Sample Output
# 24
# 10395
# 325

# Sample Input
# [4, 7, 12, 17, 19]
# [15, 4, 18, 1]
# [2, 8, 10, 14]

# [4, 7, 12, 17, 19]

numbers = 4 * 7 * 12 * 17 * 19
print(numbers)

# [15, 4, 18, 1]

numbers = 15 * 4 * 18 * 1
print(numbers)

# [2, 10, 14, 8]

numbers = 2 * 10 * 14 * 8
print(numbers)

# Sample Output

# 108528
# 1080
# 2240
