# The Last Digit
# Input a natural number N and output its last digit.
# Sample Input
# 156
# 789155
# 7
# Sample Output
# 6
# 5
# 7

# 417
# 20041219
# 3

# 417
#len(string)
s = '417'
print(len(s))

a = s[-1 : ]
print(a)

# 20041219
# len(string)
s = '20041219'
print(len(s))

a = s[-1 : ]
print(a)

# 3
# len(string)
s = '3'
print(len(s))

a = s[-1 : ]
print(a)
