# 4. Write a Python function to write a list to a file.

a_list = ["abc", "def", "ghi"]
f = open("a_file.txt", "w")
for element in a_list:
    f.write('\n' + element)
f.close()