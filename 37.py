# Import modules
# 5. Generate a random Password which meets the following conditions
# ● Password length must be 10 characters long.
# ● It must contain at least 2 upper case letters, 1 digit, and 1 special symbol.

import random
import string

for i in range(3):
    result_str_1 = " ".join(random.sample(string.ascii_lowercase, 6))
    result_str_2 = " ".join(random.sample(string.ascii_uppercase, 2))
    result_str_3 = " ".join(random.sample(string.digits, 1))
    result_str_4 = " ".join(random.sample(string.punctuation, 1))
    result_str = result_str_1 + result_str_2 + result_str_3 + result_str_4
    print(result_str)

