class Car:
    def __init__(self, weights, numbers,colors):
        self.weights = weights
        self.numbers = numbers
        self.colors = colors

    def get_car_model(self):
        return(self.weights, self.numbers, self.colors)

class Bus(Car):
    def __init__(self, weights, numbers, colors, seats):
        super().__init__(weights, numbers, colors)
        self.seats = seats
a = Bus(2000, '77XX770', 'black', 50)
print(a.colors, a.numbers, a.seats, a.weights)
a.get_car_model()
