# 4. Write a function display_words() in python to read lines from a text file "story.txt", and  display those words, which are less than 4 characters.

def display_words() :
    f = open("story.txt", "r")
    text_1 = f.readlines()
    for i in text_1 :
        word = i.split(" ")
        print(word)
    f.close()

print(len("Word with length less than or equal to than 4"))
display_words()
