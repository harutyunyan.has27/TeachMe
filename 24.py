# 5. Write a python program to read a file, a.txt line by line.

def line():
    f = open("a.txt", "r")
    text_1 = f.readline()
    return len(text_1.split(" \n "))

print(line())
