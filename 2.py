# 2.Count
# Count all letters, digits, and special symbols from a given string
# Sample Input
# P@#yn26at^&i5ve
# Sample Output
# Total counts of chars, digits, and symbols
# chars = 8
# digits = 3
# symbol = 4
import isdigit as isdigit

str_1 = "GeeKs01fOr@gEEks07"

digit = 0
letter = 0

for i in list(str_1):
    print(i)

    if letter and i:
        letter += 1
        print(i.letter)

    elif isdigit and i:
        isdigit += 1
        print(i.isdigit)
