# 4. Write a Python program to find intersection of two given arrays using Lambda.
# Original arrays:
# [1, 2, 3, 5, 7, 8, 9, 10]
# [1, 2, 4, 8, 9]
# Intersection of the said arrays:
# [1, 2, 8, 9]

def iterable(a, b):
    result = list(filter(lambda x: x in a, b))
    print(result)


a = [38, 3, 7,567, 8, 34, 98, 72, 849, 5679, 2387, 96427]
b = [34, 3, 567, 72, 2387, 849]
iterable(a, b)
