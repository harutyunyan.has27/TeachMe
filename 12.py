# FUNCTIONS
# 3. Ticket numbers usually consist of an even number of digits. A ticket number is considered lucky if the sum of the first half of the digits is equal to the sum of the second half. Given a ticket number n, determine if it's lucky or not.
# ● For n = 1230, the output should be
# isLucky(n) = true;
# ● For n = 239017, the output should be
# isLucky(n) = false.


a = [4536, 13898757, 79867580, 6453, 4213684]
n = len(a)

for i in range(0, n):
    k = a[i]
    if "isLucky(k)":
        print(k, " is Lucky ")
    else:
        print(k, " is not Lucky ")
