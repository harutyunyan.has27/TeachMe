# Write a Python program to check whether a given string is number or not using Lambda.

str_1 = "Hello73289375", "3346467world873894375", "Python74289748237"
# use lambda function to filter and comparing
result = list(filter(lambda a: a[0].isupper(), str_1)),(filter(lambda a: a[0].isdigit(), str_1))
# printing the result
print(result)
