# 4. Write a Python program to count the even, and odd numbers in a given array of integers using  Lambda.

list1 = [10, 21, 17, 138, 4, 45, 66, 93, 11]
odd_count = 0
even_count = 0
# noinspection PyRedeclaration
odd_count = len(list(filter(lambda x: x % 2 != 0 , list1)))
# noinspection PyRedeclaration
even_count = len(list(filter(lambda x: x % 2 == 0 , list1)))
print(even_count)
print(odd_count)
