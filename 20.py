# 5. Write a Python program to add two given lists using map and lambda.(map-y kara function-ic heto mekic avel hajordakanutyun ynduni, original erku list)
# Original list:
# [1, 2, 3]
# [4, 5, 6]
# Result: after adding two list
# [5, 7, 9]

a = [4, 7, 3]
b = [19, 12, 17]
result = map(lambda x, y: x + y, a, b)
print(list(result))