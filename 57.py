# 10.Salaries
# Given the salaries of three employees working at a department, find the amount of money by which the salary of the highest-paid employee differs from the salary of the lowest-paid employee. The input consists of three positive integers - the salaries of the employees. Output a single number, the difference between the top and the bottom salaries
# Sample Input
# 100 500 1000
# 500 100 1000
# 36  11  20
# 20  20  20
# Sample Output
# 900
# 900
# 25
# 0