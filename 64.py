# 5.Duplicates
# Write a Python program to remove duplicates from a list

# Sample Input
# [1, 2, 3, 1]
# [5, 7, 3, 9, 11]
# [25, 1, 1, 13]

# Sample Output
# [1, 2, 3]
# [5, 7, 3, 9, 11]
# [25 , 1, 13]

# Python code to remove duplicate elements
def Remove(duplicate):
    final_list = []
    for num in duplicate:
        if num not in final_list:
            final_list.append(num)
    return final_list


# Driver Code
duplicate = [2, 4, 10, 20, 5, 2, 20, 4]
print(Remove(duplicate))


duplicate = [2, 4, 10, 20, 5, 2, 20, 4]
print(list(set(duplicate)))