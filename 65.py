# 6.Square
# Turn every item of a list into its square

# Sample Input
# [5, 7, 3, 9, 11]

# Sample Output
# [25, 49, 9, 81, 121]

numbers = [4, 7, 12, 17, 19]

squared_numbers = [number ** 2 for number in numbers]

print(squared_numbers)