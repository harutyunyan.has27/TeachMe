# 8.Three Numbers
# Input three integers. Output the word “Sorted” if the numbers are listed in a non-increasing or non-decreasing order and “Unsorted” otherwise.

# Sample Input
# 1 2 3
# 1 3 2
# 5 0 -4
# 9 9 9
# 9 9 0
# Sample Output
# Sorted
# Unsorted
# Sorted
# Sorted
# Sorted


# Python program to find the largest number
# among the  three numbers using library function

# Driven code
a = 10
b = 14
c = 12
print(max(a, b, c))