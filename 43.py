class Country:

    country_type_1 = 'South_Korea'
    country_type_2 = 'Japan'
    country_type_3 = 'China'

    def __init__(self, capital, language, population):
        self.capital = capital
        self.language = language
        self.population = population

        from abc import ABC
        class Country(abc):
            def action(self):
                pass

            object_1 = South_Korea()
            object_1.action()
            object_2 = Japan()
            object_2.action()
            object_3 = China()
            object_3.action()

            # noinspection PyMethodMayBeStatic,PyPep8Naming
            class South_Korea(Country):
                def getCapital(self):
                    print('South Korea\'s capital is Seoul')

                def getLanguage(self):
                    print('South Korea\'s national language is Korean')

                def getPopulation(self):
                    print('According to the 2017 census, the population of South Korea is 51 million people')
            a = Country(South_Korea())
            print(a.capital())
            print(a.language())
            print(a.population())
            # noinspection PyMethodMayBeStatic
            class Japan(MyCountry):
                def getCapital(self):
                    print('Japan\'s capital is Tokyo')

                def getLanguage(self):
                    print('Janpan\'s national language is Japanese')

                def getPopulation(self):
                    print('According to the 2012 census, the population of Japan is 127,6 million people')
            b = MyCountry(Japan())
            print(b.capital())
            print(b.language())
            print(b.population())
            # noinspection PyMethodMayBeStatic
            class China(Country):
                def getCapital(self):
                    print('China\'s capital is Beijing')

                def getLanguage(self):
                    print('China\'s national language is Chinese')

                def getPopulation(self):
                    print('According to the 2012 census, the population of China is one milliard three hundred fifty-one million people')
            c = Country(China())
            print(c.capital())
            print(c.language())
            print(c.population())