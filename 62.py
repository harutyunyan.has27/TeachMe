# 3.Smallest
# Write a Python program to get the smallest number from a list

# Sample Input
# [1, 2, 3, 4]
# [5, 7, 3, 9, 11]
# [25, 1, 1, 13]
# [1, 2, 1, 1]

# Sample Output
# 1
# 3
# 1
# 1

# Python program to find smallest

# number in a list

# list of numbers
list1 = [4, 12, 4, 7, 17, 19]

# printing the maximum element
print("Smallest element is:", min(list1))

