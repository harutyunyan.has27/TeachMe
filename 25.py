# 1. Write a Python function to count the number of lines from a text file "story.txt" which is not starting with a letter "T".
# Sample.
# A boy is playing there.
# There is a playground.
# An aeroplane is in the sky.
# The sky is pink.
# Alphabets and numbers are allowed in the password.
# Output: 3

def line_count():
    file = open("story.txt","r")
    count = 0
    for line in file:
        if line[0] not in 'T':
            count += 1
    file.close()
    print("No of lines not starting with equal 'T' ",count)

line_count()
