# 5. Write a function in Python to count words in a text file those are ending with letter "e".

def count_words():
    f = open("a_3.txt","r")
    count = 0
    text_1 = f.read()
    words = text_1.split()
    for word in words:
        if word[-1] == 'e':
            count += 1
    print(count)
    f.close()

count_words()
