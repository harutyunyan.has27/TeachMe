# 1.Max of Three
# Write a Python function to find the Max of three numbers.
# Sample Input
# max_of_three(5, 11, 3)
# Sample Output
# 11

# Python program to find the largest
# number among the three numbers

def maximum(a, b, c):
    if (a >= b) and (a >= c):
        largest = a

    elif (b >= a) and (b >= c):
        largest = b

    else:
        largest = c

        return largest

a = 17
b = 19
c = 3

print(maximum(a, b, c))
