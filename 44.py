class Country:
    def __init__(self, area, language, population):
        self.area = area
        self.language = language
        self.population = population

    def __repr__(self):
        return f'Area == {self.area}\nLanguage == {self.language}\nPopulation == {self.population}'

    def __eq__(self, other):
        return self.area == other.area

    def __ge__(self, other):
        return self.population >= other.population

    def __lt__(self, other):
        return self.population < other.population

    def __ne__(self, other):
        return self.area != other.area

    def __le__(self, other):
        return self.population <= other.population

    def __gt__(self, other):
        return self.area > other.area

    def __add__(self, other):
        return self.population + other.population

    def __sub__(self, other):
        return self.area - other.area

    def __mul__(self, other):
        return self.area * other.area

    def __truediv__(self, other):
        return self.area / other.area

country_1 = Country(21295, 'Korean', 51000000)
country_2 = Country(12456, 'Japanese', 127600000)
print(country_1 == country_2)
print(country_1 >= country_2)
print(country_1 < country_2)
print(country_1 != country_2)
print(country_1 <= country_2)
print(country_1 > country_2)
print(country_1 + country_2)
print(country_1 - country_2)
print(country_1 * country_2)
print(country_1 / country_2)