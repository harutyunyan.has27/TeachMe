# Comprehensions
# 2. Find the common numbers in two lists (without using a tuple or set) list_a = [1, 2, 3, 4], list_b = [2, 3, 4, 5]

list_a = [17, 7, 1, 12, 36, 4, 3, 6, 8, 10]
list_b = [12, 39, 17, 7, 4, 5, 3, 68, 79, 28]
common = [a for a in list_a if a in list_b]
print(common)