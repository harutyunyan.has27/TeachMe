# Python3 program to check if sequence obtained
# by concatenating two bracket sequences
# is balanced or not.

# Check if given string is balanced bracket
# sequence or not.
def isBalanced(s):
    st = list()

    n = len(s)

    for i in range(n):

        # If current bracket is an opening
        # bracket push it to stack.
        if s[i] == '(':
            st.append(s[i])

        # If current bracket is a closing
        # bracket then pop from stack if
        # it is not empty. If stack is empty
        # then sequence is not balanced.
        else:
            if len(st) == 0:
                return False
            else:
                st.pop()

    # If stack is not empty, then sequence
    # is not balanced.
    if len(st) > 0:
        return False

    return True


# Function to check if string obtained by
# concatenating two bracket sequences is
# balanced or not.
# noinspection PyShadowingNames
def isBalancedSeq(s1, s2):
    # Check if s1 + s2 is balanced or not.
    if isBalanced(s1 + s2):
        return True

    # Check if s2 + s1 is balanced or not.
    return isBalanced(s2 + s1)


# Driver Code
if __name__ == "__main__":
    s1 = ")()(())))"
    s2 = "(()(()("

    if isBalancedSeq(s1, s2):
        print("Balanced")
    else:
        print("Not Balanced")

# This code is contributed by
# sanjeev2552