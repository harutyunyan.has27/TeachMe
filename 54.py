# 7.Birth Year
# The program prompts the user their birth year. Assuming a person’s age is a non-negative integer not exceeding 120, output the user’s age or the words “Incorrect Year”. The sample outputs assume it’s currently the year 2016. If you are writing this program during any other year, the correct answers may differ. Store the value of the current year in a variable.
# Sample Input
# 2016
# 2018
# 1903
# 1803
# 1991
# Sample Output
# 0
# Incorrect Year
# 113
# Incorrect Year
# 25

# Python3 code to  calculate age in years
from datetime import date


def calculateAge(born):
    today = date.today()
    try:
        birthday = born.replace(year=today.year)

    # raised when birthdate is February 29
    # and the current year is not a leap year
    except ValueError:
        birthday = born.replace(year=today.year,
                                month=born.month + 1, day=1)

    if birthday > today:
        return today.year - born.year - 1
    else:
        return today.year - born.year


# Driver code
print(calculateAge(date(1997, 2, 3)), "years")