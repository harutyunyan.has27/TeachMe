# 11.Rounding - 2
# Given a real number, round it to the nearest whole.
# Sample Input
# 0.3
# 1.2398
# 1.5
# 67.567
# Sample Output
# 0
# 1
# 2
# 68


a_float = 19.17159265
rounded_float = round(a_float, 2)
print(rounded_float)
