# 2. Write a Python program to find if a given string starts with a given character using Lambda.
str_1 = "Hello", "world", "Python"
result = map(lambda a: a[0].isupper(), str_1)
print(list(result))
