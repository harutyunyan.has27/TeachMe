# Comprehensions
# 6.  Write a list comprehension to print this output, without using mul (*)- x * ‘*‘’
# *
# **
# ***
# ****
# *****
# ******
# *******
# ********
# *********

lst = '\n'.join([x * '*' for x in range(20)])
print(lst)